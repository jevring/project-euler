package net.jevring.exercises.euler;

import java.util.ArrayList;
import java.util.List;

/**
 * Problem 3.
 *
 * @author markus@jevring.net
 */
public class LargestPrimeFactor {
	/*
	The prime factors of 13195 are 5, 7, 13 and 29.

	What is the largest prime factor of the number 600851475143 ?
	 */
	public static void main(String[] args) {
		for (int i = 1; i <= 100; i++) {
			if (isPrime(i)) {
				System.out.print(i + " ");
			}
			//System.out.printf("%d is prime %b\n", i, isPrime(i));
		}
		System.out.println();
		System.out.printf("Factors of %d: %s\n", 13195, factor(13195).toString());
		System.out.printf("Factors of %d: %s\n", 13195, factor(600851475143L).toString());
	}

	/**
	 * Factorizes a number. Finds the primes that a value can be divided with.
	 */
	private static List<Long> factor(long v) {
		List<Long> factors = new ArrayList<>();
		long prime = 2;
		long remainder = v;
		while (true) {
			prime = nextPrime(prime);
			if (remainder % prime == 0) {
				factors.add(prime);
				remainder = remainder / prime;
				if (isPrime(remainder)) {
					factors.add(remainder);
					break;
				}
			}
		}
		
		return factors;
	}

	private static long nextPrime(long prime) {
		long p = prime + 1;
		while (!isPrime(p)) {
			p += 1;
		}
		return p;
	}

	/**
	 * A value is a prime if it is <b>only</b> divisible by 1 or itself.
	 */
	private static boolean isPrime(long v) {
		long limit = (long) Math.floor(Math.sqrt(v));
		for (long i = 2; i <= limit; i++) {
			if (v % i == 0) {
				return false;
			}
		}
		return true;
	}
}
