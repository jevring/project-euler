package net.jevring.exercises.euler;

import java.util.ArrayList;
import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class TenThousandAndFirstPrime {
	/*
	

	By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
	
	What is the 10 001st prime number?

	 */

	public static void main(String[] args) {
		// lets assume we've already found 2
		int primesFound = 1;
		int p = 3;
		while (true) {
			if (isPrime(p)) {
				primesFound++;
				System.out.println("Prime " + primesFound + " is " + p);
				
				if (primesFound == 10001) {
					break;
				}
			}

			p++;
		}
		System.out.println(p);
	}

	/**
	 * A value is a prime if it is <b>only</b> divisible by 1 or itself.
	 */
	private static boolean isPrime(long v) {
		// todo: this discards 2 as being a prime. wtf?
		long limit = (long) Math.ceil(Math.sqrt(v));
		for (long i = 2; i <= limit; i++) {
			if (v % i == 0) {
				return false;
			}
		}
		return true;
	}
	
}
