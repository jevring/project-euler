package net.jevring.exercises.euler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Problem 4.
 *
 * @author markus@jevring.net
 */
public class LargestPalindromeProduct {
	/*
	

	A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
	
	Find the largest palindrome made from the product of two 3-digit numbers.

	 */
	public static void main(String[] args) {
		System.out.println("isPalindrome(9009) = " + isPalindrome(9009));
		System.out.println("isPalindrome(9001) = " + isPalindrome(9001));
		System.out.println("isPalindrome(90009) = " + isPalindrome(90009));
		List<Integer> palindromes = new ArrayList<>(); 
		for (int i = 999; i > 99; i--) {
			for (int j = 999; j > 99; j--) {
				if (isPalindrome(i * j)) {
					palindromes.add(i * j);
				}
			}
		}
		Collections.sort(palindromes);
		System.out.println("Palindromes: " + palindromes);
		System.out.println("Largest palindrome: " + palindromes.get(palindromes.size() - 1));
	}
	
	public static boolean isPalindrome(int v) {
		String s = String.valueOf(v);
		for (int i = 0; i < s.length(); i++) { // todo: only need to go through half the string if it's a palindrome
			if (s.charAt(i) != s.charAt(s.length() - i - 1)) {
				return false;
			}
		}
		return true;
	}
}
