package net.jevring.exercises.euler;

/**
 * Problem 5.
 *
 * @author markus@jevring.net
 */
public class SmallestMultiple {
	/*
	

	2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
	
	What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

	 */

	public static void main(String[] args) {
		int i = 0;
		while (true) {
			if (isDivisibleByAllNumbersFrom1To(20, ++i)) {
				System.out.println(i);
				break;
			}
		}
	}

	private static boolean isDivisibleByAllNumbersFrom1To(int limit, int value) {
		for (int i = 1; i <= limit; i++) {
			if (value % i != 0) {
				return false;
			}
		}
		return true;
	}
}
