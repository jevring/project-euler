package net.jevring.exercises.euler;

/**
 * Problem 6.
 *
 * @author markus@jevring.net
 */
public class SumSquareDifference {
	/*
	

	The sum of the squares of the first ten natural numbers is,
	1^2 + 2^2 + ... + 10^2 = 385
	
	The square of the sum of the first ten natural numbers is,
	(1 + 2 + ... + 10)^2 = 552 = 3025
	
	Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
	
	Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

	 */

	public static void main(String[] args) {
		System.out.println("sumOfSquares(10) = " + sumOfSquares(10));
		System.out.println("squareOfSum(10) = " + squareOfSum(10));
		System.out.println("difference = " + (squareOfSum(10) - sumOfSquares(10)));
		System.out.println("sumOfSquares(100) = " + sumOfSquares(100));
		System.out.println("squareOfSum(100) = " + squareOfSum(100));
		System.out.println("difference = " + (squareOfSum(100) - sumOfSquares(100)));
	}
	
	private static int sumOfSquares(int v) {
		int sum = 0;
		for (int i = 1; i <= v; i++) {
			sum += (i * i);
		}
		return sum;
	}

	private static int squareOfSum(int v) {
		int sum = 0;
		for (int i = 1; i <= v; i++) {
			sum += i;
		}
		return sum * sum;
	}
}
