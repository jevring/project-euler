package net.jevring.exercises.euler;

/**
 * Problem 9.
 *
 * @author markus@jevring.net
 */
public class SpecialPythagoreanTriplet {
	/*
	

		A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
		a^2 + b^2 = c^2
		
		For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
		
		There exists exactly one Pythagorean triplet for which a + b + c = 1000.
		Find the product abc.

	 */
	public static void main(String[] args) {
		for (int a = 1; a < 1000; a++) {
			for (int b = a; b < 1000; b++) {
				for (int c = b; c < 1000; c++) {
					if (isPythagoeran(a, b, c) && (a + b + c) == 1000) {
						System.out.printf("a = %d, b = %d, c = %d, product = %d", a, b, c, (a * b * c));
					}
				}
			}
		}
	}
	
	private static boolean isPythagoeran(int a, int b, int c) {
		int a2 = a * a;
		int b2 = b * b;
		int c2 = c * c;
		return a2 + b2 == c2;
		
	}
}
