package net.jevring.exercises.euler;

/**
 * Problem 10.
 *
 * @author markus@jevring.net
 */
public class SummationOfPrimes {
	/*
	The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

	Find the sum of all the primes below two million.
	 */

	public static void main(String[] args) {
		int limit = 2000000;
		long sum = 0; // NOTE: This is sneaky! If you make this an int, you get overflow and you wrap around, which produces the wrong result
		for (int i = 2; i < limit; i++) {
			if (isPrime(i)) {
				sum += i;
			}
		}
		System.out.println(sum);
	}

	/**
	 * A value is a prime if it is <b>only</b> divisible by 1 or itself.
	 */
	private static boolean isPrime(long v) {
		long limit = (long) Math.floor(Math.sqrt(v));
		for (long i = 2; i <= limit; i++) {
			if (v % i == 0) {
				return false;
			}
		}
		return true;
	}
}
