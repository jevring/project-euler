# Project Euler
These are some exercises from https://projecteuler.net/
The class names aren't numbered, but the docs should specify which numbered problem each of them solves.